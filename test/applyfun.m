function out = applyfun(varargin)
% applyfun(@(x)2*x, [1,2])
% applyfun(@(x)2*x, {1,2})
% applyfun(@(x)2*x, {1,2},'UniformOutput', false)
% applyfun(@(x)2*x, struct('a1', 1,'a2',2))
% applyfun(@(x)2*x, struct('a1', 1,'a2',2),'UniformOutput', false)
if nargin < 2
    error('missing input: args start with  "function_handle, inst", and possible options.');
end

switch class(varargin{2})
    case 'cell',    out = cellfun(varargin{:}); %'cell';
    case 'double',  out = arrayfun(varargin{:}); %'array';
    case 'struct',  out = structfun(varargin{:}); %'struct';
    otherwise,      error('Nopenopenope, no can do.');
end

% More fun: using strings
% funcHandle = str2func([out 'fun']);
% funcHandle(varargin{:});
end