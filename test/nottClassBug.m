classdef nottClassBug < handle 
    %%% Code to observe the bug
    % n = nottClassBug;
    % open n;
    % n.setNewProp('newValue'); 
    %%% n.myNewProp hasn't changed in variable editor
    %%% close variable editor
    % open n; 
    %%% this time the value has changed
    
    properties ( SetAccess = private )
        myNewProperty@char   = 'initialValue'
    end
    
    methods 
        function setNewProp( obj, value )
            % Only way to change property myNewProperty
            obj.myNewProperty = value;
        end
    end
end
