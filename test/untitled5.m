opt.a = 1;
opt.b = 2;
opt.c = 3;
% defining a=opt.a, b=opt.b,.. at once (in base workspace only).
%cellfun(@(s)eval(sprintf('%s=opt.(%s)', s, s)),fields(opt));
% cellfun(@(s)(eval([s '=opt.' s])),fields(opt)) % not ok because variables defined within function cellfun!
% eval(sprintf('%s=opt.%s;', 'a','a'))
cellfun(@(s)(eval(['assignin(''base'',''' s ''',evalin(''base'',''opt.' s '''))'])),fields(opt)) % not ok because variables defined within function cellfun!
% deal(fields(opt))
eval(sprintf('%s = opt.%s;', fieldnames(opt), fieldnames(opt)))